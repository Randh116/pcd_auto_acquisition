@echo off

:interface
echo Select mode:
echo 1) Recording and processing
echo 2) Only processing
set /p select_mode=" "

if %select_mode%==1 goto record
if %select_mode%==2 (goto process) else (goto err_invalid_value)

:record
set /p filename="Enter the name of file: "
set /p frames="Number of frames to record: "
echo %frames%
set /p select_visualize="Show all frames? (y/n) "
if %select_visualize%==y goto rec
if %select_visualize%==n (goto rec_in_range) else (goto err_invalid_value)
	
:process
set /p is_exported="Already exported from .rrf to .ply? (y/n) "
if %is_exported%==n goto export
if %is_exported%==y (set /p filename="Enter the name of file: ") else (goto err_invalid_value)
if exist %filename% (echo Correct file.) else (goto err_invalid_filename)

rem count how many files are in the directory 
for /f %%a in ('2^>nul dir "%filename%" /a-d/b/-o/-p/s^|find /v /c ""') do set n=%%a
set frames=%n%
set /p select_visualize="Show all frames? (y/n) "
if %select_visualize%==y (goto visualize) else (goto visualize_in_range)

:export
set /p filename="Enter the filename without .rrf extension: "
set filename_rrf=%filename%.rrf
if exist %filename_rrf% (echo Correct .rrf file.) else (goto err_invalid_filename)
sampleExportPLY.exe %filename_rrf%
md %filename%
move *.ply %filename%
rem counts all the files in directory to know how many frames this .rrf file had
for /f %%a in ('2^>nul dir "%filename%" /a-d/b/-o/-p/s^|find /v /c ""') do set n=%%a
set frames=%n%
move %filename_rrf% %filename%

:visualize
visualize.py -i %filename% -f %frames% -a
goto again

:visualize_in_range
set /p select_if_single="Show only one single frame? (y/n) "
if %select_if_single%==y goto visualize_single
set /p begin_frame="1st frame to visualize: "
set /p end_frame="last frame to visualize: "
visualize.py -i %filename% -f %frames% -r -bf %begin_frame% -lf %end_frame%
goto again


:visualize_single
set /p frame_number="number of the frame to show: "
visualize.py -i %filename% -f %frames% -s -sf %frame_number%
goto again

:rec
visualize.py -p -a -i %filename% -f %frames%
goto again 

:rec_in_range
set /p select_if_single="Show only one single frame? (y/n) "
if %select_if_single%==y goto record_vis_single
set /p begin_frame="1st frame to visualize: "
set /p end_frame="last frame to visualize: "
visualize.py -i %filename% -f %frames% -p -r -bf %begin_frame% -lf %end_frame%
goto again

:record_vis_single
set /p frame_number="number of the frame to show: "
visualize.py -i %filename% -f %frames% -p -s -sf %frame_number%
goto again

:err_invalid_filename
echo Error: File %filename% does not exist. Enter a valid filename.
goto again

:err_invalid_value
echo Error: Invalid value. Enter a valid value.
goto again

:again
set /p select="Run again? (y/n) "
cls
if %select%==y goto interface
if %select%==n (goto end_script) else (goto err_invalid_value)

:end_script
exit

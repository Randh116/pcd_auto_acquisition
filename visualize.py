import argparse
import open3d as o3d
import os
import subprocess

def main():
    parser = argparse.ArgumentParser(description='Arguments, to properly process the visual data')
    parser.add_argument('-i', '--input', type=str, required=True,
                        help='Name of the file without extension - if not exists, script will create it')
    parser.add_argument('-f', '--frames', type=int, nargs='?',
                        help='Total frames, that:\n'
                             'a) are in the file (when -p false)\n'
                             'b) should be recorded (if -p true)')
    parser.add_argument('-p', '--process', action='store_true',
                        help='Process: record and export the data before visualizing')
    viewMode = parser.add_mutually_exclusive_group()
    viewMode.add_argument('-r', '--range', action='store_true',
                          help='Show frames in range')
    viewMode.add_argument('-a', '--all', action='store_true',
                          help='Show all frames without range')
    viewMode.add_argument('-s', '--single', action='store_true',
                          help='Show single frame')
    parser.add_argument('-bf', '--beginframe', type=int, nargs='?',
                        help='Number of frame, from which visualizing should begin (in use with -r only)')
    parser.add_argument('-lf', '--lastframe', type=int, nargs='?',
                        help='Number of frame, at which visualising should end (in use with -r only)')
    parser.add_argument('-sf', '--selectedframe', type=int, nargs='?',
                        help='Number of the single frame that should be shown (in use with -s only)')

    options = parser.parse_args()

    if (options.range or options.all or options.single) and options.process:
        record_data(options, options.input, options.frames)
    else:
        choose_viewmode(options)


def record_data(parserData, input, framesAmount):
    filename_rrf = input + ".rrf"
    print("The script will record {amount} frames".format(amount=framesAmount))
    print("Name of the catalog with data: {name}".format(name=input))
    print("Name of the recording file: {name}".format(name=input))

    try:
        os.mkdir(input)
        print("directory ", input, " created")
    except FileExistsError:
        print("directory ", input, " already exists")

    # THE BUGGY PART!!! - old data not to use
    #os.system('cmd /k "break>{name}"'.format(name=filename_rrf))
    #os.system('cmd /k "sample_record_rrf.py --frames {amount} --output {name}"'.format(amount=framesAmount, name=filename_rrf))
    #os.system('cmd /k "sampleExportPLY.exe {name}"'.format(name=filename_rrf))
    #os.system('break>test.rrf | sample_record_rrf.py --frames 10 --output test.rrf | sampleExportPLY.exe test.rrf')
    #os.mknod("test.rrf")
    # END BUGGY PART

    # REPAIRED
    subprocess.run(['break', '>', filename_rrf], shell=True)
    subprocess.run(['sample_record_rrf.py', '--frames', str(framesAmount), '--output', filename_rrf], shell=True)
    subprocess.run(['sampleExportPLY.exe', filename_rrf], shell=True)
    subprocess.run(['move', '*.ply', input], shell=True)
    subprocess.run(['move', filename_rrf, input], shell=True)

    # tried to make it with shutil module - doesnt work
    #shutil.move(filename_rrf, input)

    choose_viewmode(parserData)


def choose_viewmode(options):
    if options.range:
        if options.beginframe == None and options.lastframe == None:
            print("error: you should specify the -bf and -lf param!")
        else:
            visualize_in_range(options.input, options.frames, options.beginframe, options.lastframe)
    elif options.all:
         visualize_all(options.input, options.frames)
    elif options.single:
        if options.selectedframe == None:
            print("error: you should specify the -sf param!")
        else:
            visualize_single_frame(options.input, options.frames, options.selectedframe)
    else:
        visualize_all(options.input, options.frames)


def visualize_all(input, framesAmount):
    print("Visualizing all frames.")
    frameCounter = 1
    cwd = os.getcwd()
    fullpath = cwd + "\\" + input + "\\"
    filename = fullpath + str(frameCounter) + ".ply"

    while frameCounter <= framesAmount:
        frame_visualized = o3d.io.read_point_cloud(filename)
        frame_visualized.paint_uniform_color([1, 0.706, 0])
        o3d.visualization.draw_geometries([frame_visualized])
        frameCounter += 1

def visualize_single_frame(input, framesAmount, selected_frame):
    print("showing a single frame nr {sf}".format(sf=selected_frame))
    cwd = os.getcwd()
    fullpath = cwd + "\\" + input + "\\"
    filename = fullpath + str(selected_frame) + ".ply"

    if framesAmount >= selected_frame:
        frame_visualized = o3d.io.read_point_cloud(filename)
        frame_visualized.paint_uniform_color([1, 0.706, 0])
        o3d.visualization.draw_geometries([frame_visualized])
    else:
        print ("error: input a valid frame number. it should be lower than {amount}".format(amount = framesAmount))

def visualize_in_range(input, framesAmount, first_frame, last_frame):
    print("showing frames in range from {bf} to {lf}".format(bf=first_frame, lf=last_frame))
    frameCounter = first_frame
    cwd = os.getcwd()
    fullpath = cwd + "\\" + input + "\\"
    filename = fullpath + str(first_frame) + ".ply"

    if 0 < first_frame < last_frame <= framesAmount:
        while frameCounter <= last_frame:
            frame_visualized = o3d.io.read_point_cloud(filename)
            frame_visualized.paint_uniform_color([1, 0.706, 0])
            o3d.visualization.draw_geometries([frame_visualized])
            frameCounter += 1


if __name__ == "__main__":
    main()
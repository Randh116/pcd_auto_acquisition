
this is a script to automate some recording and exportation processes during collecting data with the camera. it simplifies the work - you can record data and automatically store it in a folder in an extracted format.

you can use one of this modes:
1) record, export and visualize
2) export from .rrf and visualize
3) just visualize

visualisation has also three modes:
1) all frames
2) frames in range
3) one single frame

to start just run START_ME.bat
the script has a weak input validation, also can cause plenty of errors, so just be aware of putting the right data

if you want to use some scripts separately, just try to use the argparse help:

sample_record_rrf.py -h
visualize.py -h